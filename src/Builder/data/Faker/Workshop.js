import Materials from "@/Builder/data/Faker/Materials";

export default class Workshop {
    static get User() {
        const Name = Materials.get_name();
        return {
            gender: Name.gender,
            firstname: Name.firstname,
            lastname: Name.lastname,
            birthdate : Materials.birthDate,
            Password: Materials.password,
            email: Materials.email,
            address:  Materials.street,
            city: Materials.city,
            postalCode: Materials.zipcode,
            phone: Materials.phoneNumber,
            cardOwner : Name.lastname + " " + Name.firstname,
            cardNumber: Materials.card_number,
            cardCcv : Materials.ccv,
            cardExpireDate: Materials.expiration_date
            //CreditCard: this.get_CreditCard(Name.firstname + " " + Name.lastname)
        }
    }
    static get_CreditCard(name) {
        return {
            cardOwner : name,
            cardNumber: Materials.card_number,
            cardCcv : Materials.ccv,
            cardExpireDate: Materials.expiration_date
        }
    }
    static get Store() {
        return {
            Uuid: Materials.uuid, //uuid car en attente de joindre sa a la bdd
            Name: Materials.city,
            /*Geo: {
                Lat: Materials.latitude,
                Lng:Materials.longitude
            }*/
            Lat: Materials.latitude,
            Lng:Materials.longitude
        }
    }
    static get Stock() {
        return Materials.Number
    }
    static get Category() {
        return {
            Uuid: Materials.uuid, //uuid car en attente de joindre sa a la bdd
            Name: Materials.Product,
            Department: Materials.ProductDep,
        }
    }
    static get Product() {
        return {
            Uuid: Materials.uuid,
            Name: Materials.ProductName,
            Price: Materials.ProductPrice,
            Description: Materials.ProductDescription,
            Image: Materials.Picture
        }
    }
    static get Discount() {
        const Date = Materials.TimeDiscount;
        return {
            Percentage: Materials.Discount,
            DateTime: Date.refDay,
            StartDate: Date.beginDate,
            EndDate: Date.endDate
        }
    }
    static get Comment() {
        const res = Materials.Comment;
        return {
            Stars: res.stars,
            Text: res.comment,
        }
    }
    static get TrueFalse() {
        return Materials.Boolean
    }
}