class CommentBuilder {
    faker = require('faker/locale/fr');
    build(status){
        let je = "Je " + this.buildJe(status);
        let j = "J'" + this.j(status);
        let c = "C'est de " + this.c(status);
        let lep = "Produit de " + this.builddetres(status);
        let staln = this.buildj(status);

        return this.faker.random.arrayElement([
            je,
            j,
            staln,
            c,
            lep,
            staln
        ]);
    }

    builddetres(status) {
        let phrase = this.adjectif(status);
        if(this.faker.random.boolean()){
            phrase += "je " + this.verbe(status)
        }
        if(this.faker.random.boolean()){
            phrase += this.produit();
        }
        return phrase;
    }
    buildJe(status){
        let phrase = this.verbe(status);
        //console.log(this.faker.random.boolean();
        if(this.faker.random.boolean()){
            phrase += this.produit();
            if(this.faker.random.boolean()){
                phrase += "de " + this.adjectif(status)
            }
        }
        return phrase;
    }
    buildj(status){
        let phrase = this.standAlone(status);
        if(this.faker.random.boolean()){
            phrase += this.produit();
            if(this.faker.random.boolean()){
                phrase += ", je " + this.verbe(status)
            }
        }
        return phrase;
    }
    c(status){
        let phrase = "";
        if(this.faker.random.boolean()){
            phrase += "très ";
        }
        phrase += this.adjectif(status);
        if(this.faker.random.boolean()){
            phrase += "je " + this.verbe(status)
            if(this.faker.random.boolean()){
                phrase += this.produit()
            }
        }
        return phrase;
    }
    j(status){
        let phrase = this.apostroph(status);
        if(this.faker.random.boolean()){
            phrase += this.produit()
        }
        if(this.faker.random.boolean()){
            phrase += " je " + this.verbe(status);
        }

        return phrase
    }
    produit (){
        return this.faker.random.arrayElement([
            "ce produit ",
            "ce truc ",
            "comme produit ",
            "comme truc ",
            "cette chose ",
            "commme chose ",
            "cette matière ",
            "commme matière ",
            "commme denrée ",
            "cette denrée ",
            "cet article ",
            "comme article "
        ]);
    }
    standAlone(status){
        const bien = this.faker.random.arrayElement([
            "super ",
            "superbe ",
            "essentiel ",
            "genial "
        ]);
        const moyen = this.faker.random.arrayElement([
            "moyen ",
            "bof ",
            "passable ",
            "pas top ",
            "pas super ",
            "déçu ",
        ]);
        const mauvais = this.faker.random.arrayElement([
            "pas top ",
            "pas super ",
            "déçu ",
            "horrible ",
        ]);
        if(status === 0){
            //bad review
            return mauvais;
        }
        else if(status === 1){
            //medium review
            return moyen;
        }else {
            //good review
            return bien;
        }
    }
    adjectif(status){
        const bien = this.faker.random.arrayElement([
            "bon goût ",
            "bonne facture ",
            "bonne qualité ",
        ]);
        const mauvais = this.faker.random.arrayElement([
            "mauvais goût ",
            "mauvaise facture ",
            "mauvaise qualité ",
        ]);
        if(status === 0){
            //bad review
            return mauvais;
        }
        else if(status === 1){
            //medium review
            return mauvais;
        }else {
            //good review
            return bien;
        }

    }
    apostroph(status){
       const bien = this.faker.random.arrayElement([
            "adore ",
            "approuve ",
            "aime ",
        ]);
        const mauvais = this.faker.random.arrayElement([
            "aime pas trop ",
            "aime pas du tout ",
        ]);
        if(status === 0){
            //bad review
            return mauvais;
        }
        else if(status === 1){
            //medium review
            return mauvais;
        }else {
            //good review
            return bien;
        }
    }
    verbe(status){
        const bien = this.faker.random.arrayElement([
            "kiff ",
            "recommande fortement ",
            "recommande ",
            "conseil ",
            "conseil fortement ",
            "suis content ",
            "ne suis pas mécontent ",
            "ne suis pas déçu "
        ]);
        const moyen = this.faker.random.arrayElement([
            "ne recommande pas ",
            "déconseil ",
            "n'aime pas beacuoup ",
            "suis déçu ",
            "ne suis pas très content ",
            "n'aime pas trop ",
            "n'aime pas vraiment ",
        ]);
        const mauvais = this.faker.random.arrayElement([
            "ne recommande pas ",
            "ne recommmande pas du tout ",
            "déconseil ",
            "déconseil fortement ",
            "déteste ",
            "n'aime pas ",
            "n'aime pas du tout ",
            "suis fortement déçu ",
            "suis déçu ",
            "suis mécontent ",
            "ne suis pas content ",
        ]);
        if(status === 0){
            //bad review
            return mauvais;
        }
        else if(status === 1){
            //medium review
            return moyen;
        }else {
            //good review
            return bien;
        }
    }
}
export default new CommentBuilder()