import Workshop from './Workshop'

export default class Factory {
    static get_User(comp = 1) {
        return this.Builder(comp, "User");
    }
    static get_Comment() {
        return Workshop.Comment;
    }

    static get_Store(comp = 1) {
        return this.Builder(comp, "Store");
    }
    static get_stock() {
        return Workshop.Stock;
    }

    static  get_Category(comp = 1) {
        return this.Builder(comp, "Category");
    }

    static get_Product(comp = 1) {
        return this.Builder(comp, "Product");
    }

    static get_Discount() {
        return Workshop.Discount;
    }

    static get_TrueFalse(comp) {
        return this.Builder(comp, "TrueFalse");
    }

    static Builder(comp, object) {
        const temp = [];
        for (let i = 0; i < comp; i++) {
            temp.push(Workshop[object])
        }
        return temp;
    }
}