import CommentBuilder from "@/Builder/data/Faker/CommentBuilder";

export default class Materials {
    static faker = require('faker/locale/fr');

    static get uuid(){
        return this.faker.random.uuid()
    }
    static get_gender_sex(gender){
        if (gender === 0) {
            return "Male"
        } else {
            return "Female"
        }
    }
    static get gender () {
        return this.faker.random.number(1);
    }
    static get_name(){
        const gender = this.gender;
        return {firstname : this.get_firstname(gender), lastname: this.get_last_name(gender), gender: this.get_gender_sex(gender)};
    }
    static  get_firstname(gender){
        return this.faker.name.firstName(gender);
    }
    static get_last_name(){
        return this.faker.name.lastName();
    }
    static get find_name(){
        return this.faker.name.findName();
    }
    static get username() {
        return this.faker.internet.userName();
    }
    static get password() {
        return this.faker.internet.password();
    }
    static get email() {
        return this.faker.internet.email();
    }
    static get street() {
        return  this.Number + " " + this.faker.address.streetPrefix() + " " + this.faker.address.streetSuffix()
    }
    static get city() {
        return this.faker.address.city();
    }
    static get zipcode() {
        return this.faker.address.zipCode();
    }
    static get latitude() {
        return this.faker.address.latitude()
    }
    static get longitude() {
        return this.faker.address.longitude()
    }
    static get phoneNumber() {
        return this.faker.phone.phoneNumber()
    }
    static get birthDate() {
        const oldest = new Date('1965,12,31');

        const newest = new Date('2002,12,31');

        return this.faker.date.between(oldest, newest);
    }
    static get card_number() {
        return this.faker.finance.account(4) +'-'+ this.faker.finance.account(4) + '-' + this.faker.finance.account(4) +'-'+ this.faker.finance.account(4)
    }
    static get ccv() {
        return this.get_Number(100, 999)
    }
    static get expiration_date() {
        let month = this.faker.random.number({min: 1, max: 12});
        if (month < 10){
            month = "0" + month
        }
        return new Date(this.faker.random.number({min: 2020, max: 2025}) + "," + month + ", 01");
    }
    static get ProductName() {
        return this.faker.commerce.productName();
    }
    static get ProductPrice() {
        return this.faker.commerce.price(0.99, 60, 0.5);
    }
    static get ProductDescription() {
        return this.faker.lorem.text();
    }
    static get Product() {
        return this.faker.commerce.product();
    }
    static get ProductDep () {
        return this.faker.commerce.department();
    }
    static get Picture(){
        return this.faker.image.image()
    }
    static get Boolean() {
        return this.faker.random.boolean();
    }
    static get Number() {
        return this.faker.random.number({min: 0, max: 100})
    }
    static get Discount() {
        return this.faker.random.number({min: 2, max: 30})
    }
    static get Stars() {
        return this.faker.random.number({min: 1, max: 5})
    }
    static get DateTime() {
        return Date()
    }
    static get TimeDiscount() {
        const EndDate = this.faker.date.future();
        return {refDay: this.DateTime, beginDate: this.faker.date.between(Date(), EndDate), endDate: EndDate }
    }
    static get Comment() {
        const status = this.get_Number(0,3);
        let stars = 1;
        if(status === 0){
            //bad review
            stars = this.get_Number(1,2)
        }
        else if(status === 1){
            //medium review
            stars = this.get_Number(2,3)
        }else {
            //good review
            stars = this.get_Number(3,5)
        }
        return {stars: stars, comment: this._build_comment(status)};
    }
    static getInArray(list, number){
        let temp = [];
        for (let i = 0; i < number; i++) {
            let test = this.faker.random.arrayElement(list);
            temp.push(test);
        }
        return temp;
    }
    static _build_comment(status) {
        return CommentBuilder.build(status);
    }

    static get_Number(min, max) {
        return this.faker.random.number({min: min, max: max})
    }


}
