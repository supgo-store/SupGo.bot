import API from "@/Builder/data/API/API";
import User from "@/Builder/User/User_Builder";
import Stock_Builder from "@/Builder/Store/Stock_Builder";
import Product_Builder from "@/Builder/Store/Product/Product_Builder";
import Store_Builder from "@/Builder/Store/Store_Builder";
import Actions_Builder from "@/Builder/User/Actions_Builder";
import Discount_Builder from "@/Builder/Store/Product/Discount_Builder";
import Store from "@/store";
import Category from "@/Builder/Store/Product/Category_Builder";

export default class ApiManager {
    static async GetAllUsers() {
        const Users = await API.GetUser();
        Store.dispatch('SetAllData', {data: new User(Users.length, Users) , type: ['user']});
    }
    static async CreateUser(UserList){
        return await API.CreateUser(UserList);
    }
    static async GetStock(){
        const items = await API.GetStock();
        /////Tres moche j'ai tres honte mais je ne sais pas comment améliorer ça et c'est aps faut d'avoir essayer
        let Products = {};
        items.forEach((item) => {
            if (item.quantity !== 0){
                Products[item.productUuid] = {};
            }
        });
        items.forEach((item) => {
            if (item.quantity !== 0){
                Products[item.productUuid][item.storeUuid] = new Stock_Builder(item.uuid, item.productUuid, item.storeUuid, item.quantity);
            }
        });
        Store.dispatch('SetAllData', {data: Products , type: ['stock']});
    }
    static async GetProducts(){
        Store.dispatch('SetAllData', {data: Product_Builder.Builder(await API.GetProducts()) , type: ['product']});
    }

    static async GetStore(){
        Store.dispatch('SetAllData', {data: new Store_Builder(await API.GetStore()), type: ['store']});
    }

    static async GetActions(){
        Store.dispatch('SetAllData', {data: new Actions_Builder(await API.GetActions()), type: ['actions']});
    }
    static async GetDiscount(){
        const items = await API.GetDiscount();
        if(items){
            let temp = {};
            items.forEach((item) => {
                if(this.InStock(item.childProduct.uuid) && this.IsPromotionActive(item.startDate, item.endDate)){
                    temp[item.childProduct.uuid] = new Discount_Builder(item, item.childProduct.uuid)
                }
            });
            Store.dispatch('SetAllData', {data: temp, type: ['discount']});
        }
    }
    static InStock(IdProduct){
        return !!Store.getters.getStock[IdProduct];
    }
    static IsPromotionActive(StartDate, EndDate){
        return new Date() > new Date(StartDate) && new Date() < new Date(EndDate);
    }
    static async GetCategory(){
        Store.dispatch('SetAllData', {data: new Category(await API.GetCategory()), type: ['category']});
    }
}