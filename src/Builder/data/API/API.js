import UserService from "@/Builder/data/API/services/UserService";
import CategoryService from "@/Builder/data/API/services/CategoryService";
import ProductService from "@/Builder/data/API/services/ProductService";
import ActionsService from "@/Builder/data/API/services/ActionsService";
import ShopService from "@/Builder/data/API/services/ShopService";
import StockService from "@/Builder/data/API/services/StockService";
import DiscountService from "@/Builder/data/API/services/DiscountService";
import CartService from "@/Builder/data/API/services/CartService";
import OrderService from "@/Builder/data/API/services/OrderService";
import HistoryService from "@/Builder/data/API/services/HistoryService";
import CommentService from "@/Builder/data/API/services/CommentService";

export default class API {
    static async GetUser(){
        try {
            let response = await UserService.index();
            return response.data;
        } catch (e) {
            console.error(e)
        }
    }
    static async CreateUser(User){
        try {
            let response = await UserService.create(User);
            return response.data;
        } catch (e) {
            console.error(e)
        }
    }
    static async GetCategory(){
        try {
            let response = await CategoryService.index();
            return response.data;
        } catch (e) {
            console.error(e)
        }
    }
    static async GetProducts(){
        try {
            let response = await ProductService.index();
            return response.data;
        } catch (e) {
            console.error(e)
        }
    }
    static async GetActions(){
        try {
            let response = await ActionsService.index();
            return response.data;
        } catch (e) {
            console.error(e)
        }

    }
    static async GetStore(){
        try {
            let response = await ShopService.index();
            return response.data;
        } catch (e) {
            console.error(e)
        }
    }
    static async GetStock(){
        try {
            let response = await StockService.index();
            return response.data
        } catch (e) {
            console.error(e)
        }
    }
    static async GetDiscount(){
        try {
            let response = await DiscountService.index();
            return response.data;
        }catch (e) {
            console.error(e)
        }
    }
    static async CreateCart(Cart){
        try {
            let response = await CartService.create(Cart);
            return response.data;

        }catch (e) {
            console.error(e)
        }
    }
    static UpdateCart(OldCart, NewCart){
        try {
            //console.log("Update Cart", OldCart, NewCart)
            CartService.update(OldCart, NewCart);
        }catch (e) {
            console.error(e)
        }
    }
    static CreateOrder(Order){
        try {
            //console.log("CreateOrder", Order);
            OrderService.create(Order);
        }catch (e) {
            console.error(e)
        }
    }
    static UpdateStock(Stock){
        try {
            //console.log("UpdateStock", Stock, Stock.Quantity);
            StockService.update(Stock, Stock.Quantity)
        }catch (e) {
            console.error(e)
        }
    }
    static async WriteHistory(History){//History
        try {
            HistoryService.create(History);
        }catch (e) {
            console.error(e)
        }
    }
    static WriteComment(comment){
        //write in db comment
        try {
            CommentService.create(comment);
        }catch (e) {
            console.error(e)
        }
    }
}