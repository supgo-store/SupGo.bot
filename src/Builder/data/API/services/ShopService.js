import { api } from '@/Builder/data/API/services/Api.js'

export default {
  index () {
    return api.get(`/api/Store`)
  }
}
