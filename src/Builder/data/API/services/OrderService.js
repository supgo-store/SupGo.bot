import { api } from '@/Builder/data/API/services/Api.js'
export default {
  index () {
    return api.get(`/api/Orders`)
  },
  create (newItem) {
    return api.post(`/api/Orders`, newItem)
  },
  indexByCart (uuid) {
    return api.post(`/api/Orders/filtered-list`, {
      'Key': 'CartUuid',
      'Value': uuid
    })
  }
}
