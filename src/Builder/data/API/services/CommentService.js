import { api } from '@/Builder/data/API/services/Api.js'

export default {
  index () {
    return api.get(`/api/Comment`)
  },
  create(comment){
    return api.post(`/api/Comment`, comment)
  },
  delete (uuid) {
    return api.delete(`/api/Comment/${uuid}`)
  }
}
