import { api } from '@/Builder/data/API/services/Api.js'

export default {
  index () {
    return api.get(`/api/Cart`)
  },
  indexByCart(uuid) {
    return api.get(`/api/Cart/${uuid}`);
  },
  create (newItem) {
    return api.post(`/api/Cart`, newItem)
  },
  update (cart, newData) {
    cart.TotalPrice = newData.TotalPrice;
    cart.DateTime = newData.DateTime;
    return api.put(`/api/Cart`, cart)
  },
}
