import { api } from '@/Builder/data/API/services/Api.js'

export default {
    index () {
        return api.get(`/api/History`)
    },
    create (newItem) {
        return api.post(`/api/History`, newItem)
    },
}
