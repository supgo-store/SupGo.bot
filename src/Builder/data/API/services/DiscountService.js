import { api } from '@/Builder/data/API/services/Api.js'

export default {
  index () {
    return api.get(`/api/Discount`)
  },
  update (newItem) {
    return api.put(`/api/Discount`, newItem)
  },
  create (newItem) {
    return api.post(`/api/Discount`, newItem)
  },
  delete (uuid) {
    return api.delete(`/api/Discount/${uuid}`)
  }
}
