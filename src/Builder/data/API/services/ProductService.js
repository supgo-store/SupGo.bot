import { api } from '@/Builder/data/API/services/Api.js'

export default {
  index () {
    return api.get(`/api/Product`)
  },
  indexByCategory (uuid) {
    return api.post(`/api/Product/filtered-list`, {
      'Key': 'CategoryUuid',
      'Value': uuid
    })
  },
  update (newItem) {
    return api.put(`/api/Product`, newItem)
  },
  create (newItem) {
    return api.post(`/api/Product`, newItem)
  },
  delete (uuid) {
    return api.delete(`/api/Product/${uuid}`)
  }
}
