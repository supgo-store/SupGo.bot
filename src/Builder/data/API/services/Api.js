import Axios from 'axios'
import securityService from '@/Builder/data/API/services/SecurityService.js'
import store from '@/store/index'
import Vue from 'vue'
import Moment from 'moment'
import env from '@/components/config/config.js'
export const auth = Axios.create({
  baseURL: env.keycloakURL
})

export const api = Axios.create({
  baseURL: env.apiURL
})

async function interceptRequest (config) {
  if (!store.state.keycloak || !store.state.keycloak.token || !store.state.keycloak.refreshToken || (Vue.$jwt.decode(store.state.keycloak.refreshToken).exp - Moment().unix()) < 0) {
    store.dispatch('keycloak', null)
    return {
      ...config,
      cancelToken: new Axios.CancelToken((cancel) => cancel('Auth required'))
    }
  } else if ((Vue.$jwt.decode(store.state.keycloak.token).exp - Moment().unix()) < 0) {
    try {
      let response = await securityService.refreshToken(store.state.keycloak.refreshToken)
      console.log('llll')
      store.dispatch('setToken', response.data.access_token)
      store.dispatch('setRefreshToken', response.data.refresh_token)
    } catch (error) {
      console.error(error)
      store.dispatch('keycloak', null)
      var url = `${env.keycloakURL}/auth/realms/${env.realm}/protocol/openid-connect/logout?redirect_uri=${encodeURIComponent(`${env.frontURL}/login`)}`
      window.location.href = url
    }
  }
  config.headers = {
    Authorization: store.state.keycloak && store.state.keycloak.token ? `Bearer ${store.state.keycloak.token}` : ''
  }
  return config
}

/*

async function errorResponse (error) {
  switch (error.response.status) {
    case 401:
      // invalid token: if exist it must be removed
      if (store.state.userToken) {
        store.dispatch('logout', {
          tenant: null,
          company: {},
          tenants: [],
          userToken: null,
          user: {},
          error: false,
          expiration: true
        })
      }
      router.push('login')
      break
    case 400:
      store.dispatch('setInvalidRequest', true)
      return Promise.reject(error)
    case 403:
      if (error.config.params && error.config.params.preventError) {
        return null
      } else if (error.config.params && error.config.params.preventNotify) {
        return Promise.reject(error)
      } else {
        store.dispatch('setForbidden', true)
        return Promise.reject(error)
      }
    case 500:
      store.dispatch('setInternalError', true)
      return Promise.reject(error)
    default:
      return Promise.reject(error)
  }
}

api.interceptors.response.use((response) => {
  return response
}, (error) => errorResponse(error)
)
*/
api.interceptors.request.use((config) => interceptRequest(config))
