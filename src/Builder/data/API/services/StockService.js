import { api } from '@/Builder/data/API/services/Api.js'

export default {
  index () {
    return api.get(`/api/Stock`)
  },
  update (stock, quantity) {
    return api.put(`/api/Stock`,
      {
        idStock: stock.idStock,
        productUuid: stock.IdProduct,
        quantity: quantity,
        storeUuid: stock.IdStore,
        uuid: stock.Uuid
      }
    )
  }
}
