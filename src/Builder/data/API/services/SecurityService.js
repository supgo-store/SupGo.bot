import { auth } from '@/Builder/data/API/services/Api.js'
import qs from 'qs'
import config from '@/components/config/config.js'

export default {
  refreshToken (refreshtoken) {
    return auth.post(`/auth/realms/${config.realm}/protocol/openid-connect/token`, qs.stringify({
      grant_type: 'refresh_token',
      refresh_token: refreshtoken,
      client_id: config.clientId
    }),
    {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
  }
}
