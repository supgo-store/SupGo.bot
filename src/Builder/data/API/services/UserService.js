import { api } from '@/Builder/data/API/services/Api.js'

export default {
  index () {
    return api.get(`/api/User`)
  },
  get (uuid) {
    return api.get(`/api/User/${uuid}`)
  },
  create (newItem) {
    return api.post(`/api/User`, newItem)
  },
  update (user, newData) {
    user.address = newData.address
    user.phone = newData.phone
    user.birthdate = newData.birthdate
    user.city = newData.city
    user.gender = newData.gender
    user.postalCode = newData.postalCode
    return api.put(`/api/User`, user)
  },
  updatePayment (user, newdata) {
    user.cardCcv = newdata.cardCcv
    user.cardOwner = newdata.cardOwner
    user.cardExpireDate = newdata.cardExpireDate
    user.cardNumber = newdata.cardNumber
    return api.put(`/api/User`, user)
  }
}
