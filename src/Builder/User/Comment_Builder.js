import Comment from "@/models/User/Comment";
import Factory from "@/Builder/data/Faker/Factory";

export default class Comment_Builder {
    constructor(IdProduct, IdUser, data = Factory.get_Comment()) {
        return new Comment(data.Stars, data.Text, IdProduct, IdUser);
    }
}