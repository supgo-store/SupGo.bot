import User from "@/models/User/User";
import Factory from "@/Builder/data/Faker/Factory";

export default class User_Builder {
    constructor(comp, data = Factory.get_User(comp)) {
        return this.Builder(data);
    }
    Builder(object) {
        const temp = [];
        object.forEach(function(data) {
            temp.push(new User(data.uuid, data.gender, data.firstname,data.lastname, data.birthdate, data.Password, data.email, data.address, data.city, data.postalCode, data.phone, data.cardCcv, data.cardExpireDate, data.cardNumber, data.cardOwner, data.cart, data.comment, data.History))
        });
        return temp;
    }
}