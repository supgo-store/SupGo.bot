import Action from "@/models/Actions/Action";

export default class Actions_Builder {
    constructor(data) {
        return this.Builder(data);
    }
    Builder(object) {
        const temp = [];
        object.forEach(function(data) {
            temp.push(new Action(data.uuid, data.actionType))
        });
        return temp;
    }
}