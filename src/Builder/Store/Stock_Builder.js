import Factory from "@/Builder/data/Faker/Factory";
import Stock from "@/models/Store/Stock.js";

export default class Stock_Builder {
    constructor(IdStock, IdProduct, IdStore, Quantity = Factory.get_stock()) { //possibilte de mettre le nombre desire si rien mis -> genere une valeur 0-100
        return new Stock(IdStock, IdProduct, IdStore, Quantity)
    }
}