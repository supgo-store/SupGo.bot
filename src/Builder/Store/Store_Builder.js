import Factory from "@/Builder/data/Faker/Factory";
import Store from "@/models/Store/Store.js";
import Mechanic from "@/Scripts/Machine/Mechanic";

export default class Store_Builder {
    constructor(data = Factory.get_Store(1)) {
        return this.Builder(data);
    }

    Builder(object) {
        const temp = [];
        object.forEach(async function (data) {
            temp.push(new Store(data.uuid, data.name, data.latitude, data.longitude, Mechanic.GetCategoryStore(data.uuid)))
        });
        return temp;
    }
}