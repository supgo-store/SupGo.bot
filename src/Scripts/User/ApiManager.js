import Mechanic from "@/Scripts/Machine/Mechanic";
import API from "@/Builder/data/API/API";
import History from "@/models/Actions/History";
import Cart from "@/models/Actions/Cart";
import Store from "@/store";

export default class ApiManager {
    static async EnterStore(UserUuid, StoreUuid, ProductUuid){
        console.log("Enter Store", UserUuid, new Date());
        //
        this.WriteHistory(await Mechanic.convertActionNameToUuid("EnterStore", UserUuid), UserUuid, StoreUuid, ProductUuid);
        Store.dispatch('setUserHistory', {data: "Entre dans le magasin", UserUuid: UserUuid, StoreUuid: StoreUuid});
    }
    static async LeaveStore(UserUuid, StoreUuid, ProductUuid){
        this.WriteHistory(await Mechanic.convertActionNameToUuid("LeaveStore", UserUuid), UserUuid, StoreUuid, ProductUuid);
        //
        Store.dispatch('setUserHistory', {data: "Sors du magasin", UserUuid: UserUuid, StoreUuid: StoreUuid});
        console.log("LeaveStore", UserUuid, new Date());
    }
    static async EnterDepartment(UserUuid, StoreUuid, ProductUuid){
        this.WriteHistory(await Mechanic.convertActionNameToUuid("EnterDepartment", UserUuid), UserUuid, StoreUuid, ProductUuid);
        //
        Store.dispatch('setUserHistory', {data: "Entre dans le rayon", UserUuid: UserUuid, StoreUuid: StoreUuid});
        console.log("Enter department", UserUuid, new Date());
    }
    static async LeaveDepartment(UserUuid, StoreUuid, ProductUuid){
        this.WriteHistory(await Mechanic.convertActionNameToUuid("LeaveDepartment", UserUuid), UserUuid, StoreUuid, ProductUuid);
        //
        Store.dispatch('setUserHistory', {data: "Sors du rayon", UserUuid: UserUuid, StoreUuid: StoreUuid});
        console.log("Leave department", UserUuid, new Date());
    }
    static async Article(Action, UserUuid, StoreUuid, ProductUuid){
        this.WriteHistory(await Mechanic.convertActionNameToUuid(Action, UserUuid), UserUuid, StoreUuid, ProductUuid);
        if(Action === "TakeItem"){
            Store.dispatch('setUserHistory', {data: "Prends un article", UserUuid: UserUuid, StoreUuid: StoreUuid});
        }else if (Action === "PutItem"){
            Store.dispatch('setUserHistory', {data: "Repose un article", UserUuid: UserUuid, StoreUuid: StoreUuid});
        }else if(Action === "KeepItem"){
            Store.dispatch('setUserHistory', {data: "Garde un article", UserUuid: UserUuid, StoreUuid: StoreUuid});
        }
        console.log(Action, UserUuid);
    }
    static UpdateStock(Stock){
        API.UpdateStock(Stock)
    }
    static CreateOrder(Order){
        API.CreateOrder(Order)
    }
    static async CreateCart(IdUser, IdStore){
        return new Cart(await API.CreateCart({
            UserUuid: IdUser,
            StoreUuid: IdStore,
            DateTime: null,
            TotalPrice: 0
        }), IdUser, IdStore, 0);
    }
    static UpdateCart(cart, TotalPrice){
        API.UpdateCart(cart, new Cart(cart.Uuid, cart.UserUuid, cart.StoreUuid, TotalPrice));
    }

    static WriteHistory(ActionUuid, UserUuid, StoreUuid, ProductUuid){
        API.WriteHistory(new History(ActionUuid, UserUuid, StoreUuid, ProductUuid))
    }
    static WriteComment(comment){
        API.WriteComment(comment);
    }
}
