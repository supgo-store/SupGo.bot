export default class History {
    constructor(ActionUuid, UserUuid, StoreUuid, ProductUuid) {
        this.ActionUuid = ActionUuid;
        this.UserUuid = UserUuid;
        this.StoreUuid = StoreUuid;
        this.ProductUuid = ProductUuid;
        this.DateTime = new Date();
    }
}