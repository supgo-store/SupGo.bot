export default class Comment {
    constructor(NbStars, Text, IdProduct, IdUser) {
        this.DateTime = new Date();
        this.NbStars = NbStars;
        this.Text = Text;
        this.ProductUuid = IdProduct;
        this.UserUuid = IdUser;
    }
}