export default class Category {
    constructor(Uuid, Name, Department, IdStore, Product) {
        this.Uuid = Uuid;
        this.Name = Name;
        this.Department = Department;
        this.IdStore = IdStore;
        this.Product = Product;
    }
}