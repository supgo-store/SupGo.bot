export default class Discount {
    constructor(IdDiscount, Percentage, StartDate, EndDate, IdProduct) {
        this.IdDiscount = IdDiscount;
        this.Percentage = Percentage;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.IdProduct = IdProduct;
    }
}