export default class Product {
    constructor(IdProduct, Name, Price, Description, IdCategory, Image) {
        this.IdProduct = IdProduct;
        this.Name = Name;
        this.Price = Price;
        this.Description = Description;
        this.IdCategory = IdCategory;
        this.Image = Image;
    }
}