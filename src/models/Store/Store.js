export default class Store {
    constructor(IdStore, Name, Latitude, Longitude, Categories) {
        this.IdStore = IdStore;
        this.Name = Name;
        this.Longitude = Longitude;
        this.Latitude = Latitude;
        this.Categories = Categories;
    }
}