export default class Stock {
    constructor(Uuid, IdProduct, IdStore, Quantity) {
        this.Uuid = Uuid;
        this.Quantity = Quantity;
        this.IdProduct = IdProduct;
        this.IdStore = IdStore;
    }
}