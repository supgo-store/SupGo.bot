const config = {
  keycloakURL: 'http://auth.supgo-market.fr',
  frontURL: 'http://supgo-market.fr:8082',
  apiURL: 'http://api.supgo-market.fr',
  mailSupport: 'noreply.supgo@gmail.com',
  realm: 'SupGo',
  clientId: 'supgo-web'
}

export default config
