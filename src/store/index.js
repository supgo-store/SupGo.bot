import Vue from 'vue'
import Vuex from 'vuex'
import PersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    keycloak: null,
    webSocket: null,
    data: {user: null, actions: null, stock: null, discount: null, category: null, product: null, store: null},
    cart: {},
  },
  mutations: {
    keycloak (state, keycloak) {
      state.keycloak = keycloak
    },
    setToken (state, token) {
      state.keycloak.token = token;
      state.keycloak.tokenParsed = Vue.$jwt.decode(token)
    },
    setRefreshToken (state, refreshToken) {
      state.keycloak.refreshToken = refreshToken;
      state.keycloak.refreshTokenParsed = Vue.$jwt.decode(refreshToken)
    },
    setData: (state, object) => {
        state.data[object.type] = object.data
    },
    setWebSocket(state, ws){
      state.webSocket = ws
    },
    setUserHistory:(state, object) => {
      state.data.user.forEach((usr)=> {
        if(usr.uuid === object.UserUuid){
          if(usr.History[object.StoreUuid]){
            usr.History[object.StoreUuid].push(object.data)
          }else{
            usr.History[object.StoreUuid] = [object.data]
          }
        }
      })
    },
    setCart:(state, object) => {
      let temp = {};
      temp[object.cart.Uuid] = object.cart;
      state.cart[object.UserId] = temp;
    },
    setOrders: (state, object) => { ///UserID : [order, order, ...], ...
      state.cart[object.UserId][object.order.CartUuid].Order.push(object.order)
    },
    updateQuantity: (state, object) => {
      state.data.stock[object.IdProduct][object.IdStore].Quantity = object.Quantity
    }
  },
  actions: {
    keycloak (context, keycloak) {
      context.commit('keycloak', keycloak)
    },
    setToken (context, token) {
      context.commit('setToken', token)
    },
    setRefreshToken (context, refreshToken) {
      context.commit('setRefreshToken', refreshToken)
    },
    setWebSocket(context, refreshToken){
      context.commit('setWebSocket', refreshToken)
    },
    SetAllData: ({commit}, object) => {
      commit('setData', object)
    },
    setCart:({commit}, object) => {
      commit('setCart', object)
    },
    setOrders: ({commit}, object) => {
      commit('setOrders', object)
    },
    updateStock: ({commit}, object) => {
      commit('updateQuantity', object)
    },
    setUserHistory: ({commit}, object)=>{
      commit('setUserHistory', object)
    }
  },
  getters: {
    getKeycloak: state => {
      return state.keycloak
    },
    getWs: state => {
      return state.webSocket
    },
    getUser: state => {
      return state.data.user
    },
    getActions: state => {
      return state.data.actions
    },
    getProduct: state => {
      return state.data.product
    },
    getCategory: state => {
      return state.data.category
    },
    getStore: state => {
      return state.data.store
    },
    getStock: state => {
      return state.data.stock
    },
    getDiscount: state => {
      return state.data.discount
    },
    getCart: state => (UserId, CartId) => {
      return state.cart[UserId][CartId]
    },
    getOrders: state => (UserId, CartId) => {
      return state.cart[UserId][CartId].Order
    },
    getUserHistory: state => (UserId, StoreUuid) => {
      state.data.user.forEach((usr)=> {
        if(usr.uuid === UserId){
          return usr.History[StoreUuid];
        }
      })
    },
  },
  plugins: [PersistedState()]
})
