const WebSocketServer = require("ws").Server;
const ws = new WebSocketServer( { port: 8100 } );

console.log("Server started...");
ws.on('connection', function (ws) {
    console.log("Browser connected online...");
    ws.on("message", function (str) {
        const ob = JSON.parse(str);
        switch(ob.type) {
            case 'text':
                console.log("Received: " + ob.content);
                ws.send('{ "type":"text", "content":"Server ready."}');
                break;
            default:
                console.log("Received: " + '"type":'+ob.type+', '+ob.type+':'+ob.content+'');
                ws.send('{ "type":'+ob.type+', '+ob.type+':'+ob.content+'}');
                break;
        }
    });

    ws.on("close", function() {
        console.log("Browser gone.")
    })
});