import Vue from 'vue'
import App from './App.vue'
import store from './store/index'
import VueJWT from 'vuejs-jwt'


import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.config.productionTip = false
Vue.use(VueJWT)

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
